
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.

from twisted.internet import pollreactor
pollreactor.install()

from twisted.internet import reactor, protocol, defer
from twisted.enterprise import adbapi

import psycopg2

import sys
import os
import getopt

sys.path.append('./Classes');

### My Classes ###
from Algo import *
from PeerInfo import *
from VidAsServer import *
from DBInter import *
from InterServ import *

# Called when a loclist couldn't be created
#class LocListError():
#	pass

class VidAsFactory(protocol.ServerFactory):
	# Passing DB interface object
	def __init__(self,db):
		self.db = db

def getAlgo(val):
	orderingResults = "ORDER BY sps ASC"
	
	if val == "cs":
		orderingResults = "ORDER BY critical_score ASC "
	
	if val == "sps":
		orderingResults = "ORDER BY sps ASC "
	
	if val == "upa":
		orderingResults = "ORDER BY upa DESC "
	
	if val == "cssps":
		orderingResults = "ORDER BY critical_score,sps ASC "
	
	if val == "spscs":
		orderingResults = "ORDER BY sps,critical_score ASC "
	
	if val == "spsupa":
		orderingResults = "ORDER BY sps ASC, upa DESC "
	
	if val == "upasps":
		orderingResults = "ORDER BY upa DESC, sps ASC "
	
	if val == "csupa":
		orderingResults = "ORDER BY critical_score ASC, upa DESC "
	
	if val == "upacs":
		orderingResults = "ORDER BY upa DESC, critical_score ASC "
	
	return orderingResults

	
def main(argv=None):
	if argv is None:
		argv = sys.argv
	try:
		opts, args = getopt.getopt(argv[1:], "tu:a:", ["algo"])
	except getopt.error, msg:
		raise Usage(msg)
	
	# option processing
	for option, value in opts:
		if option == "-a":
			Algo.orderingResults = getAlgo(value)
		if option == "-u":
			VidAsServer.simnum = int(value)
			DBInter.dbname = "supernode_"+str(int(VidAsServer.simnum))
		if option == "-t":
			VidAsServer.t = 1
			DBInter.dbname = "supernode_v"+str(int(VidAsServer.simnum))

	Algo.selectedAlgo = "basic"
	db 	= DBInter()
	factory = VidAsFactory(db)
	factory.protocol 	= VidAsServer
	factory.conn_per_peer 	= 4
	
	print "DB : ",DBInter.dbname

	if(VidAsServer.t==1):
		print "Listening on : ",(VidAsServer.simnum+2100)
		reactor.listenTCP(VidAsServer.simnum + 2100,factory)
	else:
		print "Listening on : ",(VidAsServer.simnum+2000)
		factory.interserv = InterServ(VidAsServer.simnum + 2200)
		reactor.listenTCP(VidAsServer.simnum + 2000,factory)

	reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
	main()
