
from twisted.internet import reactor, protocol, defer
from twisted.enterprise import adbapi

import psycopg2
import cPickle as pickle
import sys
import os.path
sys.path.append('.');

### My Classes ###
from Algo import *
from PeerInfo import *
from DBInter import *
from Memory import *

class VidAsServer(protocol.Protocol):
	
	t = 0
	init=0
	simnum = 0
	
	
	def myinit(self):
		self.peer	=	None
		self.db		=	self.factory.db
		self.abg	=	[1,0,0]
		self.dumb_counter =	0
		
	def updateabg(self,a=1,b=0,c=0):
		# abg = [1,0,0]
		# try:
		# 	if(os.path.exists('/tmp/abg.p')):
		# 		abg = pickle.load(open("/tmp/abg.p","rb"))
		# 		print "Updating abg"
		# 		os.path.remove('/tmp/abg.p')
		# except OSError as e:
		# 	pass
		Memory.abg(a,b,c)
		
	# Node tries to connect
	def connectionMade(self):
		if(VidAsServer.init==0):
			VidAsServer.init=1
			Memory.create(50000,self.t)     #TODO : Get number of contents from DB
			if(VidAsServer.t == 0):
				Memory.intercomm.goSim(1)
		self.myinit()
		self.transport.write("100 WHO ?\n")

	# Node sends info
	def dataReceived(self, data):
		parsed = data.split(" ")
		
		code = parsed[0]
		
		if(code == "SERV"):
			self.transport.write("710 YOP \n")
			print "Serv connected"
			return

		if(code == "YOP"):
			print("Connection Made");
			return

		if(code == "IDEN"):
			# Create peer
			self.peer = PeerInfo(parsed[1])

			# Connect Node in the DB
			self.db.connectNode(self.peer.Id)
			self.transport.write("110 Welcome \n")
			return
			
		if(code == "FIND"):
			if(parsed[1] != ''):
				Memory.update(int(parsed[1]))
			self.db.getContent(self.peer.Id,parsed[1]).addCallback(self.sendResult)
			return
#			self.dumb_counter = self.dumb_counter + 1
#			if(self.dumb_counter == 100):
#				self.dumb_counter = 0
#				self.updateabg()
		
	
		if(code == "ACK"):
			self.db.gotAck(self.peer.Id,parsed[2])
			return
		
		if(code == "HOUR"):
			self.newHour()
			self.transport.write("402 ABG "+str(Memory.alpha)+" "+str(Memory.beta)+" "+str(Memory.gamma)+"\n")
			return

		if(code == "CLEAR"):
			print "Cleanup Time !"
			self.db.cleanDB()
			Memory.failures = 0
			return
		
		if(code == "EOT"):
			print "Received EOT"
			#TESTING
			#CLEARALL
			return
			
			if(VidAsServer.t == 0):
				return

			p=Memory.next()
			print Memory.step
			print len(Memory.choices)
			if(Memory.step > 0):
				self.db.cleanDB()
				self.transport.write("777 One More Time \n")
			else:
				self.transport.write("666 Stop " + " ".join(str(x) for x in p) + " \n")
			return

		if(code == "MYABG"):
			print "Golly, I should update my ABG"
			self.updateabg(int(parsed[1]),int(parsed[2]),int(parsed[3]))
			return
		

	def newHour(self):
		print "New Hour, Failures so far : "+str(Memory.failures)
		#TESTING
		Memory.newHour()

	def sendResult(self,data):
		if(data!=None):
			self.transport.write(data)
