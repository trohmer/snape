from twisted.internet import reactor, protocol, defer
from twisted.enterprise import adbapi

import psycopg2

### My Classes ###

class LocListError():
	pass

class csPeer():

	def __init__(self,peer,cs):
		self.peer = peer
		self.cs   = cs

	def __cmp__(self,other):
		return cmp(self.cs,other.cs)

class Algo():
	
	orderingResults = "ORDER BY ord DESC"
	selectedAlgo	= "basic"

	num_cat = 10
	hours_per_day = 7
	history = []
	hour = 0
	currentHour = [0] * num_cat
	currentDay = []
	Values = []

	def __init__(self,AlgoName):
		# Setup various algorithms
		# for i in range(0,Algo.hours_per_day):
		# 			Algo.currentHour=[0]*Algo.num_cat
		# 			Algo.currentDay.append(Algo.currentHour)
		# 			v = [0] * Algo.num_cat
		# 			Algo.Values.append(v)
		pass
		#Algo.history.append(Algo.currentDay)
		#Algo.currentDay = []

	@classmethod		
	def newHour(self):
		if(Algo.hour == Algo.hours_per_day):
			Algo.hour=0
			Algo.history.append(Algo.currentDay)
			Algo.currentDay = []
			print "==================="
			print Algo.history
		Algo.currentDay.append(Algo.currentHour)
		Algo.Values[Algo.hour] = [sum(pair) for pair in zip(Algo.Values[Algo.hour], Algo.currentHour)] 
		Algo.currentHour = [0] * Algo.num_cat
		Algo.hour = Algo.hour + 1
		

	# process list of potential peers, and return "the good ones"
	@classmethod
	def process(self,txn,potential, conn_per_peer):
		if(Algo.selectedAlgo == "basic"):
			return Algo.basic(potential,conn_per_peer)
		if(Algo.selectedAlgo == "cslive"):
			return Algo.cslive(txn,potential,conn_per_peer)
		if(Algo.selectedAlgo == "machine"):
			return Algo.machine(txn,potential,conn_per_peer)



	@classmethod
	def basic(self,potential,conn_per_peer):
		selected=[]
		all_selected=[]

		complete = 10
		part_size = 0 
		
		# Parse for each part
		for i in range(len(potential)):
			selectedForPart = []
			if(complete < part_size):
				raise LocListError

			complete = 0
			part_size=potential[i][0][5]

			# Each peer for the part
			for seeder in potential[i]:
				# Do not take twice the same peer for a content -- TODO : edit
				if(seeder[0] in all_selected):
					continue
				
				# Add peer
				selectedForPart.append(seeder)
				all_selected.append(seeder[0])
				
				#Increase completion by upa at seeder
				complete = complete +  seeder[1] / conn_per_peer
				if complete >= part_size:
					selected.append(selectedForPart)
					break
		if(len(selected) < len(potential)):
			raise LocListError

		return selected

	@classmethod
	def machine(self,txn,potential,conn_per_peer):
		selected=[]
		all_selected=[]

		complete = 10
		part_size = 0 
		
		machineList=[]

		# Parse for each part
		for i in range(len(potential)):
			selectedForPart = []
			if(complete < part_size):
				raise LocListError

			complete = 0
			part_size=potential[i][0][5]

			# Each peer for the part
			for seeder in potential[i]:
				# Do not take twice the same peer for a content -- TODO : edit
				if(seeder[0] in all_selected):
					continue

				value = 0
				parts = Algo.CanIHaz(txn,seeder[0])
				for part in parts:
					#value = value + history[part%(Algo.num_cat)]
					ContentId = int(part.split('_')[0])
					value = value + Algo.Values[ContentId%Algo.num_cat]
				machineList.append(csPeer(seeder,value))

			machineList.sort()

			for seeder in machineList:
				# Add peer
				selectedForPart.append(seeder.peer)
				all_selected.append(seeder.peer[0])
		
			#Increase completion by upa at seeder
			complete += seeder.peer[1] / conn_per_peer

			if complete >= part_size:
				selected.append(selectedForPart)
				break
		
		if(len(selected) < len(potential)):
			raise LocListError

		print "stop algo"
		return selected



	@classmethod
	def cslive(self,txn,potential,conn_per_peer):
		selected=[]
		all_selected=[]

		complete = 10
		part_size = 0 
		
		print "start algo"
		print len(potential)
		# Parse for each part
		for i in range(len(potential)):
			cslive 	= []
			selectedForPart = []
			if(complete < part_size):
				raise LocListError

			complete = 0
			part_size=potential[i][0][5]

			# Each peer for the part
			for seeder in potential[i]:
				# Do not take twice the same peer for a content -- TODO : edit
				if(seeder[0] in all_selected):
					continue
				liveCS = Algo.liveCS(txn,seeder[0])
				cslive.append(csPeer(seeder,liveCS))

			cslive.sort()

			for seeder in cslive:
				# Add peer
				selectedForPart.append(seeder.peer)
				all_selected.append(seeder.peer[0])
			
				#Increase completion by upa at seeder
				complete += seeder.peer[1] / conn_per_peer

				if complete >= part_size:
					selected.append(selectedForPart)
					break
		
		if(len(selected) < len(potential)):
			raise LocListError

		print "stop algo"
		return selected

	@classmethod
	def liveCS(self,txn,PeerId):
		req = " SELECT count(*) \
				FROM (\
				SELECT have_all.part_id FROM\
				have as have_peer\
				right OUTER JOIN have as have_all\
				ON have_peer.part_id = have_all.part_id\
				left OUTER JOIN peer as p\
				ON have_all.peer_id = p.id\
				GROUP BY  have_all.part_id, have_all.peer_id, have_peer.peer_id, p.upa, p.up\
				HAVING have_peer.peer_id = 15 AND p.upa > p.up / 4\
				) as plip\
				GROUP BY plip.part_id"
		txn.execute(str(req))

		ret     = txn.fetchall()
		livecs  = 0.0
		for r in ret[0]:
				livecs += 1.0 / float(r)
		return livecs

	@classmethod
	def CanIHaz(self,txn,PeerId):
		req = "SELECT part_id FROM have WHERE peer_id="+str(PeerId)
		txn.execute(str(req))
		return txn.fetchall()
		

	@classmethod
	def addFailure(self,PartId):
		# ContentId = int(PartId.split('_')[0])
		# cat = ContentId%Algo.num_cat
		# Algo.currentHour[cat] = Algo.currentHour[cat] + 1
		pass
