import sys
import math
sys.path.append('.');

### My Classes ###
from PeerInfo import *
from Algo import Algo, LocListError
import cPickle as pickle

class Test():
	alpha	=	0
	beta	=	0
	gamma	=	0

	failures = 0
	
	def __init__(self,alpha,beta,gamma,fail):
		self.alpha = alpha
		self.beta = beta
		self.gamma = gamma
		self.failures = fail
		print "Saving test with ("+str(alpha)+","+str(beta)+","+str(gamma)+"),"+str(fail)

	def __cmp__(self,other):
		return cmp(self.failures,other.failures)

class Memory():
	intercomm = 0
	queries =	[]
	month_predict = []
	daily_table = [2000,4000,5000,3000,1000,5000,4000,1000]
	
	tests	=	[]
	alpha 	= 	0
	beta	=	1
	gamma	=	0
	failures=	0
	
	n_step	=	3
	step	=	0
	
	t		=	0
	hours_per_week = 6

	w 		=	0
	weeks_per_month	= 1
	
	output	=	0
	
	choices = [[1,0,0],[0,1,0],[0,0,1]]

	b=0
	current_hour 	=	0

	@classmethod
	def create(cls,num_contents,b):
		Memory.queries = [0]*num_contents
		Memory.failures = 0
		Memory.newalpha()
		Memory.month_predict = [(int)(sum(Memory.daily_table)*Memory.hours_per_week*Memory.weeks_per_month*math.exp(-15*(i*i)/(float)(num_contents*num_contents))) for i in range(num_contents)]
		Memory.b = b
		if(b==1):
			Memory.output = open("results.txt","wb")
		else:
			Memory.queries = [(int)(sum(Memory.daily_table)*math.exp(-15*(i*i)/(float)(num_contents*num_contents))) for i in range(num_contents)]
			
	@classmethod	
	def reset(cls):
		Memory.tests = []
		Memory.queries 	= [0]*len(Memory.queries)
		Memory.failures = 0
		Memory.step = 0
	
	@classmethod
	def next(cls):
		t = Test(Memory.alpha,Memory.beta,Memory.gamma,Memory.failures)
		Memory.output.write( "["+str(t.alpha)+","+str(t.beta)+","+str(t.gamma)+"], f="+str(t.failures)+"\n" )
		Memory.tests.append(t)
		# Memory.queries 	= [0]*len(Memory.queries)
		Memory.failures = 0
		Memory.step = Memory.step + 1

		if(Memory.step < len(Memory.choices)):
			Memory.newalpha()
			return 0
		else:
			return Memory.endtest()
			
	# Here, I'll have to implement the simplex algo.
	@classmethod
	def newalpha(cls):
		choice = Memory.choices[Memory.step]
		Memory.abg(choice[0],choice[1],choice[2])
		
	@classmethod	
	def endtest(cls):
		t = min(Memory.tests)
		Memory.output.write("========== END SIMU ===========\n")
		Memory.output.write( str(t.alpha) +","+ str(t.beta) +","+ str(t.gamma) )
		Memory.output.write("===============================\n")

		p = [t.alpha,t.beta,t.gamma]	
		Memory.reset()
		return p
		
	@classmethod
	def update(cls,id):
		Memory.queries[id] = Memory.queries[id] + 1
	
	@classmethod
	def abg(cls,a,b,g):
		Memory.alpha 	= a
		Memory.beta 	= b
		Memory.gamma	= g
		print "new abg : ",str(a),str(b),str(g)
	
	@classmethod
	def newHour(cls):
		Memory.t = Memory.t+1
		Memory.current_hour = Memory.current_hour + 1
		if(Memory.t >= Memory.hours_per_week):
			Memory.newWeek()
			return True
		return False
	
	@classmethod
	def newWeek(cls):
		Memory.t = 0
		Memory.w = Memory.w + 1
		if(Memory.w >= Memory.weeks_per_month):
			Memory.w = 0
			Memory.month_predict = Memory.queries
			Memory.queries = [0] * len(Memory.queries)
		q = map(lambda x,y: (x-y / (Memory.weeks_per_month - Memory.w)), Memory.month_predict,Memory.queries)
		#pickle.dump(q,open("/tmp/queries.p","wb"))
		if(Memory.b == 0):
			Memory.intercomm.goSim(q)
