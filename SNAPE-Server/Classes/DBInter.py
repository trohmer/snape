from twisted.internet import reactor, protocol, defer
from twisted.enterprise import adbapi

import psycopg2

import sys
sys.path.append('.');

### My Classes ###
from PeerInfo import *
from Algo import Algo, LocListError
from Memory import *

conn_per_peer = 4

class DBInter():
	
	dbname = "supernode"
	
	def __init__(self):	
		print "New DBInter"
		self.conn = adbapi.ConnectionPool("psycopg2","host=localhost dbname="+DBInter.dbname+" user=supernode password=PassNodeSuper", cp_min= 1, cp_max=5)
		return self.cleanDB()

	def cleanDB(self):
		print "Cleaning DB"
		self.conn.runOperation("UPDATE peer SET connected=0, upa=up");
		self.conn.runOperation("DELETE FROM locpeer");
		print "DB as clean as an elven arse"
	
	# Connect a node
	def connectNode(self,peerId):
		return self.conn.runOperation("UPDATE peer SET connected=1 WHERE id="+str(peerId))

	def getContent(self,PeerId,ContentId):
		return self.conn.runInteraction(self._getContent,PeerId,ContentId)

	# Look for the content in the DB, then send LocList
	def _getContent(self,txn,PeerId, ContentId):
		global conn_per_peer	

		
		# Get infos for the content
		txn.execute("SELECT duration FROM part LEFT OUTER JOIN content ON content.id=part.content_id WHERE content_id='" + ContentId + "'")		
		res=txn.fetchall()

		if ( not res ):
			ret = ("520 ContentError " + ContentId + "\n")
			return ret
		
		ContentDuration = int(res[0][0])
		ContentParts	= len(res)
		
		# Get potential seeders for each part
		seeders = []
		for partNumber in range(0, ContentParts):
			partSeeders = self.getPart(ContentId,partNumber, txn)
			if( not partSeeders or len(partSeeders) == 0):
				ret = ("521 " + ContentId + " Content Overloaded\n")
				Algo.addFailure(ContentId)
				Memory.failures = Memory.failures + 1
				return ret
			seeders.append(partSeeders)
		# Algo
		try:
			selected = Algo.process(txn,seeders,conn_per_peer)
		except LocListError:
			ret = ("521 " + ContentId + " Content Overloaded\n")
			Algo.addFailure(ContentId)
			Memory.failures = Memory.failures + 1
			return ret
			
		# Update Peers 
		loclist=""
		partNum = 0

		for PartPeer in selected:

			sizePart 	= PartPeer[0][5]
			PartId 		= ContentId + "_" + str(partNum)
			partNum 	+= 1

			peerUpdateQuery="UPDATE Peer SET upa= greatest (0, upa - " + str(sizePart) + ") WHERE false "

			frame		= 0
			frames		= len(PartPeer)

			for peer in PartPeer:
				
				# 220-Peer Type PeerIP PeerID PartId  FirstFrame NumberOfPackets / FirstFrameFromPeer NumberOfPacketsFromPeer IperTenMilMovie
				peerUpdateQuery 	+= " OR id='" + str(peer[0]) + "' "
				locpeerInsertQuery 	= "INSERT INTO LocPeer (asking_peer_id,content_id,part_id,peer_id) VALUES "
				locpeerInsertQuery 	+= "('"+str(PeerId)+"','"+str(ContentId)+"','"+PartId+"','"+str(peer[0])+"')"
				loclist		 		+= "220-Peer V " + PeerInfo.IntToIp(int(peer[0])) + " " + str(PeerId)
				loclist				+= " "+ContentId+" "+PartId+" 0 10 / "+str(int(frame/frames))+" "+str(int((frame+1)/frames))+" 42\n"
				txn.execute(locpeerInsertQuery)

			txn.execute(peerUpdateQuery)

	
		# Generate LocList
		loclist = "220 LocList "+str(ContentId)+" "+str( (partNum)*10 ) +" "+ str(ContentDuration) + " AES cypher_key ALL\n" + loclist	
		return loclist	

	def getPart(self,ContentId, partNumber,txn):
		global conn_per_peer
		part_id= str(ContentId)+"_"+str(partNumber)
		
		Memory.alpha = 0
		Memory.beta = 0
		Memory.gamma = 1
		
		# Big sexy query
		req = 	"SELECT peer_id, upa, critical_score, sps, step, partinfo.upload,\
		 		( "+str(Memory.alpha)+" * upa - "+str(Memory.beta)+" * sps - "+str(Memory.gamma)+" * critical_score) as ord \
				FROM ( \
				SELECT peer_id, step, upload FROM have INNER JOIN part ON (have.part_id=part.id) \
				WHERE part_id = '" + part_id +"' \
				) AS partinfo \
				INNER JOIN ( \
				SELECT id, upa, critical_score, sps, mac FROM Peer \
				WHERE upa >= up / "+str(conn_per_peer)+" \
				) as peerinfo \
				ON ( peerinfo.id = partinfo.peer_id )" + Algo.orderingResults
		txn.execute(str(req))
		return txn.fetchall()

	def liveCS(self,txn,PeerId):
		req = "	SELECT count(*) \
				FROM (\
				SELECT have_all.part_id FROM\
				have as have_peer\
				right OUTER JOIN have as have_all\
				ON have_peer.part_id = have_all.part_id\
				left OUTER JOIN peer as p\
				ON have_all.peer_id = p.id\
				GROUP BY  have_all.part_id, have_all.peer_id, have_peer.peer_id, p.upa, p.up\
				HAVING have_peer.peer_id = 15 AND p.upa > 0\
				) as plip\
				GROUP BY plip.part_id"
		txn.execute(str(req))

		ret 	= txn.fetchall()
		livecs 	= 0.0
		for r in ret[0]:
			livecs += 1.0 / float(r)
		print livecs
		return livecs

	def gotAck(self,PeerId,ContentId):
		self.conn.runInteraction(self._gotAck,PeerId,ContentId)

	# Change stuff in the DB
	def _gotAck(self,txn,PeerId,ContentId):		
		# Select from content to get number of parts
		query =	"SELECT * FROM ("\
		"SELECT * FROM content WHERE id='" + ContentId +"') as cont \
		LEFT OUTER JOIN part on cont.id=part.content_id"

		txn.execute(query)
		res=txn.fetchall()
		ContentParts = len(res)
		
		# Select from LocPeer to get infos on seeding peers
		query  = ("SELECT peer_id FROM LocPeer WHERE asking_peer_id ='" + str( PeerId ) + "' AND content_id ='" + str(ContentId) + "'")
		txn.execute(query)
		res=txn.fetchall()
		
		if(not res):
			return
		
	
		# Create ONE query for updating seeding peers
		delquery = "DELETE FROM locpeer WHERE content_id='"+ContentId+"' AND asking_peer_id = '" + str( PeerId ) +"' AND ( false " 
		query 	 = "UPDATE peer SET upa= least(up,upa + up / " + str(ContentParts) + ") WHERE false "
		for peer in res:
			query=query + " OR id = " + str(peer[0])
			delquery=delquery + " OR peer_id = '" + str(peer[0]) + "' "
			
		delquery = delquery + ")"
		txn.execute(delquery)
		txn.execute(query)
		return

	

