
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.

from twisted.internet import pollreactor

from twisted.internet import reactor, protocol, defer
import sys
import os
import time
import random
import cPickle as pickle
sys.path.append('.');

from Memory import *

class InterServProtocol(protocol.Protocol):
	

	def __init__(self):
		Memory.intercomm = self
		pass
	
	def connectionMade(self):
		pass
	
	
	def dataReceived(self, data):
		# Display
		print "Server received "+data

		# Parse received info
		parsed	= data.split(" ")
		code	= parsed[0]

		# 100 Who?
		if(code == 'WHO'):
			self.send('ME \n')

		if(code == 'YOP'):
			print "Server connected"

		if(code == "ABG"):
			print "Updating ABG with" + parsed[1] + ", " + parsed[2] + ", " + parsed[3]
			Memory.abg(float(parsed[1]),float(parsed[2]),float(parsed[3]))



	def send(self,val):
		self.transport.write(val)
		print "Server sending "+val

	def connectionLost(self, reason):
		pass

	def goSim(self,q):
#		self.transport.write('GO ' + ' '.join(str(x) for x in q) + ' \n')
		self.transport.write('GO ' + str(Memory.current_hour) + ' \n')
		print 'Asking for Sim'

class InterServ(protocol.ClientFactory):
	protocol = InterServProtocol
	
	def __init__(self,num):
		reactor.connectTCP("localhost",num,self)

	def clientConnectionLost(self, connector, reason):
		pass

	def clientConnectionFailed(self, connector, reason):
		print "======== CONNECTION FAILED ========"
		pass
