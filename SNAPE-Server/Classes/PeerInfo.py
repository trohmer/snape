class PeerInfo():

	def __init__(self,MacAdress):
		
		self.MacAdress	= MacAdress
		self.Id			= self.MacToInt(MacAdress)

	@classmethod
	def _MacToInt(self,Mac):
		Id	=	0
		for val in Mac.split(":"):
			Id =  100 * Id + int(val)
		return str(Id)
	
	@classmethod
	def MacToInt(self,mac):
		i=0
		for n in mac.split(':'):
			i=99*i+int(n)
		return i	


	@classmethod
	def IntToIp(self, intip ):
		octet = ''
		for exp in [3,2,1,0]:
			octet = octet + str(intip / ( 256 ** exp )) + "."
			intip = intip % ( 256 ** exp )
		return(octet.rstrip('.'))	

	# Convert int into a mac adress
	@classmethod
	def _IntToMac(self, intmac):
		mac="%012d" % intmac
		for i in range(1,6):
			mac=mac[:12-2*i]+':'+mac[12-2*i:]
		return mac
		

	@classmethod
	def IntToMac(self, intmac):
			octet = ''
			for exp in [5,4,3,2,1,0]:
					if(intmac / (99 ** exp) < 10):
							octet = octet + "0" + str(intmac / (99 ** exp)) + ":"
					else:
							octet = octet + str(intmac / (99 ** exp)) + ":"
					intmac = intmac % ( 99 ** exp )
			return(octet.rstrip(':'))
