class Evaluate():

	queries = []
	rejects = []
	latency = []

	results = []

	gaussians = []

	def init():
		Evaluate.queries = []
		Evaluate.rejects = []
		Evaluate.latency = []

	def hop():
		for metric in (Evaluate.queries,Evaluate.rejects,Evaluate.latency):
			maxval = max([val[2] for val in metric])
			minval = min([val[2] for val in metric])
			for val in metric:
				val[2]	= (val[2] - minval) / (maxval - minval)

	def find_gauss():
		for metric in (Evaluate.queries,Evaluate.rejects,Evaluate.latency):
			x 		= [ r[0] for r in metric ]
			y 		= [ r[1] for r in metric ]
			densites 	= []
			histo 		= []

			densites = [ float("{0:.1f}".format(float(val))) for val in y ]

			num_densites	= float(len(densites))
			densites_set	= list(set(densites))
			densites_set.sort()

			histo = [ densites.count(val)/num_densites for val in densites_set ]

			(mu,sigma,a) = leastsq(residuals,[0.4,0.1,0.5],args=(histo,densites_set))[0]

			return (mu,sigma,a)

	def end_step():
		results.append(Evaluate.find_gauss())
		Evaluate.init()

	def evaluate():
			R = np.arange(min(densites_set),max(densites_set),0.001)
			L = [ gaussian(x,mu,sigma,a) for x in R ]
			
	
