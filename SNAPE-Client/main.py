#!/usr/bin/env python
# encoding: utf-8
"""
main.py

Created by Thibaud Rohmer on 2011-11-11.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import getopt
import os
import time
import random as rd
sys.path.append('./Classes');

### My Classes ###
from Simulator import *
from SettingsFile import *
from Logs import *
from InterServFile import *

help_message = "Usage : python main.py [-hvsPp]\n\
\t h:\t This help\n\
\t v:\t Verbose level\n\
\t s:\t Server\n\
\t p:\t Port\n\
\t S:\t Speed\n\
\t P:\t Peers"

class Usage(Exception):
	def __init__(self, msg):
		print msg

class InterServFactory(protocol.ServerFactory):
	def __init__(self):
		pass

def main(argv=None):
	if argv is None:
		argv = sys.argv
	try:
		try:
			opts, args = getopt.getopt(argv[1:], "htv:u:s:p:e:S:P:i:", ["help"])
		except getopt.error, msg:
			raise Usage(msg)
	
		# option processing
		for option, value in opts:
			if option == "-v":
				Settings.verbose = int(value)
			if option == "-s":
				Settings.server = value
			if option == "-p":
				Settings.port = int(value)
			if option == "-P":
				Settings.peers = int(value)
			if option == "-S":
				Settings.speed = int(value)
			if option == "-f":
				Settings.toFile 	= 1
				Logs.FileName 	= value
			if option == "-e":
				Settings.exp_id	= value
			if option == "-u":
				Settings.simnum = int(value)
				Settings.port = 2000 + int(value)
			if option == "-t":
				Settings.t = 1
				print "Connecting to :",(2100 + Settings.simnum)
				Settings.port = Settings.simnum + 2100
			if option in ("-h", "--help"):
				raise Usage(help_message)
	
		# For the stats
		path = "./Results/" + Settings.exp_id
		try:
			os.makedirs(path)
		except os.error:
			pass


		if(Settings.t==1):
			simtxt="sim"
		else:
			simtxt=""

		TimeNow 	= time.gmtime()
		NiceTime 	= "_" + str(TimeNow.tm_mday) + "_" + str(TimeNow.tm_hour) + "_" + str(TimeNow.tm_min) + "_" + str(TimeNow.tm_sec)
		Logs.File=open(path + "/" + Logs.FileName + str(NiceTime) + simtxt + ".log","w+")
		
		Settings.ErrorFile=open("error.log","w+")

		if(Settings.t == 1):
			factory = InterServFactory()
			factory.protocol 	= InterServ
			#Settings.intercomm = factory
			print "Connecting to :",(2200 + Settings.simnum )
			reactor.listenTCP(2200 + Settings.simnum ,factory)

		# Launching simulator
		Simulator()
		reactor.run()

		Logs.File.close()
		Settings.ErrorFile.close()
		
	except Usage, err:
		return 2


if __name__ == "__main__":
	sys.exit(main())
