#!/usr/bin/env python
# encoding: utf-8
"""
Pear.py

Created by Thibaud Rohmer on 2011-11-11.
Copyright (c) 2011 . All rights reserved.
"""


from twisted.internet import reactor, protocol
import sys
import threading
sys.path.append('.');

### My Classes
from Verbose import *
from Settings import *

# a client protocol

class PeerProtocol(protocol.Protocol):

	def connectionMade(self):
		self.transport.write('IDEN '+IntToMac(self.factory.peerId))
			
	def dataReceived(self, data):
		#print data
		# Display
		Verbose("Received "+data,0)
		
		# Parse received info
		parsed 	= data.split(" ")
		code	= parsed[0]
		
		# 100 Who?
		if(code == '100'):
			pass
			
		#101 Alive?
		if(code == '101'):
			self.transport.write('ALIVE')
		
		#110 Welcome : do nothing
		if(code == '110'):
			print "Peer logged"
			self.SendStuff()
			
		#120 Ping
		if(code == '120'):
			self.transport.write('PONG')
			
		#220 Sucessfull Loclist
		if(code == '220'):
			if(parsed[1]=="LocList"):
				self.callLater(60 * Settings.content_duration / Settings.speed, self.sendAck, parsed[2])
				Verbose(str(self.factory.peerId)+" got success for "+str(parsed[2]),4)
				
			else:
				# LocList part
				pass

		#521 Content Overloaded
		if(code == '521'):
			pass
			#Verbose(str(self.id) + " got failure fo "+str(parsed[2]),4)

	def connectionLost(self, reason):
		print "connection lost"

	def RequestContent(self):
		if(self.factory.ContentId != -1):
			self.transport.write("FIND "+self.factory.ContentId)
			Verbose(str(self.factory.peerId)+" requested "+self.factory.ContentId,3)
			self.factory.ContentId = -1
		self.RequestContent()
	
	def SendAck(self,ContentId):
		self.transport.write("ACK "+ContentId)

		
class PeerThread(protocol.ClientFactory):
	protocol = PeerProtocol
	
	def __init__(self,peerId):
		self.peerId=peerId
		self.ContentId=-1
	
	def clientConnectionFailed(self, connector, reason):
		print "Connection failed - goodbye!"
		reactor.stop()
	
	def clientConnectionLost(self, connector, reason):
		print "Connection lost - goodbye!"
		reactor.stop()
	
	
# Convert int into a mac adress
def IntToMac(intmac):
	octet = ''
	for exp in [5,4,3,2,1,0]:
			if(intmac / (99 ** exp) < 10):
					octet = octet + "0" + str(intmac / (99 ** exp)) + ":"
			else:
					octet = octet + str(intmac / (99 ** exp)) + ":"
			intmac = intmac % ( 99 ** exp )
	return(octet.rstrip(':'))


class Peer():
	
	PeerId=0
	ContentId=-1
		
	def __init__(self,peerId):
		self.peerId 	= Peer.PeerId
		self.ContentId 	= Peer.ContentId
		self.f			= PeerThread(self.peerId)
		Peer.PeerId 	+= 1
		reactor.connectTCP("threesim.ucd.ie", 1664, self.f)
		
	def run(self):
		reactor.run()

	def stop(self):
		self.f.transport.LoseConnection()
		reactor.stop()
		
	def askFor(self,ContentId):
		self.ContentId=ContentId




def main():
	for f in range(5):
		f = PeerThread()
		reactor.connectTCP("threesim.ucd.ie", 1664, f)
	reactor.run()
	
# this only runs if the module was *not* imported
if __name__ == '__main__':
	main()