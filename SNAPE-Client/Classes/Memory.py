### My Classes ###
from Verbose import *
from SettingsFile import *
from math import *
from scipy.optimize import leastsq
from scipy.stats import norm
import random as rd
from pylab import *
from numpy import *

class StratMemo():

	queries = []

	@classmethod
	def init(cls):
		StratMemo.queries = list()


class GlobalMemo():

	results = []
	normalized = []
	history = []
	currentSim = 0

	strategies = [[1,0,0],[0,1,0],[0,0,1]]

	x = list()
	y = list()

	@classmethod
	def init(cls):
		GlobalMemo.results = list()
		GlobalMemo.normalized = list()
		GlobalMemo.history = list()

	@classmethod
	def new(cls):
		GlobalMemo.history.append(StratMemo.queries)
		print "New sim"
		StratMemo.init()
		return GlobalMemo.strategies[GlobalMemo.currentSim % 3]

	@classmethod
	def evaluate(cls):
		GlobalMemo.history.append(StratMemo.queries)
		for i in range(len(GlobalMemo.history)):
			f = open("test"+str(i)+".txt","w+")
			f.write(str(GlobalMemo.history[i])+"\n")
			f.close()
		for stratresults in GlobalMemo.history:

			query_count = len(stratresults)
			success = 0
			participation = [0]*(1+Settings.peers)

			# Parsing Queries
			# [0, TimeStamp, self.peer.id,parsed[1], time.time() - TimeStamp - Logs.StartTime, particips]

			latency = 0
			for q in stratresults:
				if(q[0] == 1):
					success = success + 1
				latency = latency + float(q[4])
				for node in q[5]:
					participation[node] = participation[node] + 1

			success_rate = float(success)/query_count
			average_latency = float(latency)/query_count

			# Entropy
			entropy = 0
			total_participations = sum(participation)
			prob = [0] * (1+Settings.peers)
			
			for i in range(len(participation)):
				prob[i] = float(participation[i])/float(total_participations)

			for p in prob:
				if(p>0):
					entropy = entropy - p * math.log(p)

			GlobalMemo.results.append([success_rate,average_latency,entropy])

		for i in range(len(GlobalMemo.results)):
			temp_table = []
			for j in range(len(GlobalMemo.results[i])):
				all_val = [a[j] for a in GlobalMemo.results]
				if(min(all_val) == max(all_val)):
					temp_table.append(0)
				else:
					temp_table.append( float(GlobalMemo.results[i][j] - min(all_val)) / (max(all_val) - min(all_val))  )
			GlobalMemo.normalized.append(temp_table)

		GlobalMemo.init()
		# Choose Method
		return GlobalMemo.linear()

	@classmethod 
	def linear_eval(cls,a):
		return 0.5 * a[0] + 0.2 * a[1] + 0.2 * a[2]

	@classmethod
	def linear(cls):
		print "linear"
		result = [ GlobalMemo.linear_eval(a) for a in GlobalMemo.normalized]
		best_strat = result.index(max(result))
		return GlobalMemo.strategies[best_strat]

	@classmethod
	def hierarchical_eval(cls):
		print "hierachical"
		eps= 0.01
		strat_1=[]
		start_2=[]
		recup=[]
		for j in range(len(GlobalMemo.results)):
			met_val_F =[a[First_met] for a in GlobalMemo.results]
		for i in range(len(GlobalMemo.results)):
			if (GlobalMemo.results[i][First_met]== max(met_val)) or ((max(met_val)-GlobalMemo.results[i][First_met])<= eps):
				strat_1.apppend(i)
		
		for n in strat_1:
			recup.append(GlobalMemo.results[n])
		for k in range(len(recup)):
			met_val_S = [b[Second_met] for b in recup]
			if (recup[k][Second_met]== max(met_va_1)) or (max(met_va_1)-recup[k][Second_met] <= eps):
				strat_2.append(strat_1[k])
		return 	GlobalMemo.strategies[strat_2[0]]


	@classmethod
	def gaussian(cls,x,mu,sigma,a):
		if(a==-42):
			if(sigma==-42):
				return mu
			else:
				return mu + sigma * x
		else:
			return ((1.0*a)/(sqrt(2.0*pi*sigma))) * exp(-0.5 * ((1.0*(x-mu))/sigma)**2)

	@classmethod
	def residuals1(cls,coeffs,y,x):
		return (y - GlobalMemo.gaussian(x,coeffs[0],coeffs[1],coeffs[2]))**2

	@classmethod
	def residuals(cls,coeffs,y,x):
		return [(i-j) for i,j in zip(y, GlobalMemo.gaussian(x,coeffs[0],coeffs[1],coeffs[2]))]

	@classmethod
	def mix(cls,x,p):
		return min([ GlobalMemo.gaussian(x,mu,sigma,a) for (mu,sigma,a) in p ])


	@classmethod
	def montecarlo(cls,p):
		inside = 0.0
		rd.seed()

		R = list()
		L = list()

		X = list()
		Y = list()

		shot = 100000

		if(len(GlobalMemo.x)<100):
			print "generating globalmemo.x"
			GlobalMemo.x = [rd.random() for i in range(shot)]
			GlobalMemo.y = [rd.random() for i in range(shot)]

		for i in range(shot):
			x = GlobalMemo.x[i]
			y = GlobalMemo.y[i]

			f_x = min([GlobalMemo.gaussian(float(x),float(res[0]),float(res[1]),float(res[2])) for res in p])

			if( y < f_x ):
				inside = inside + 1.0
		return (inside / shot)


	@classmethod
	def evaluate_plaussibility(cls):
		GlobalMemo.history.append(StratMemo.queries)
		print "go plausibility"
		strat_id = 0
		results = list()
		values = list()
		
		for stratresults in GlobalMemo.history:
			print "new strat"
			strat_id = strat_id + 1
			
			query_count = len(stratresults)
			participation = [0]*(1+Settings.peers)

			#[0, TimeStamp, self.peer.id, parsed[1], time.time() - TimeStamp - Logs.StartTime, particips]
			# p(rej)

			f=open("quer.txt","w+")
			for h in stratresults:
				for v in h:
					f.write(str(v)+"\t")
				f.write("\n")
			f.close()
			
			rejection = []
			rej = []
			time = 0
			r=0
			s=0
			limit = 5
			entropy = []
			
			# Read queries
			for query in stratresults:
				if(query[0] == 1):
					# increase success
					s=s+1
					for peer_parti in query[5]:
						if(peer_parti > len(participation)):
							print "Weird participation : ",peer_parti
						else:
							participation[peer_parti] = participation[peer_parti] + 1
				if(query[0] == 0):
					r=r+1

			# Rej
			rej = float(r) / float(r+s)

			# Entropy
			total_participations = sum(participation)
			for i in range(len(participation)):
				if(total_participations > 0):
					prob[i] = float(participation[i])/float(total_participations)
			for p in prob:
				if(p>0):
					ent = ent - p * math.log(p)
			
			results.append([rej,ent])
		for i in range(len(results)):
			temp = list(results)
			temp.pop(i)
			v = 1 - (temp[0][0] + temp[1][0]) * (temp[0][1] + temp[1][1])
			values.append(v)
		print values
		return GlobalMemo.strategies[values.pos(max(values))]
		
	@classmethod
	def evaluate_bayes(cls):
		GlobalMemo.history.append(StratMemo.queries)
		print "go bayes"
		strat_id = 0
		for stratresults in GlobalMemo.history:
			print "new strat"
			strat_id = strat_id + 1
			startTime = stratresults[0][1]
			this_strat = list()

			query_count = len(stratresults)
			print query_count
			success = 0
			participation = [0]*(1+Settings.peers)

			#[0, TimeStamp, self.peer.id, parsed[1], time.time() - TimeStamp - Logs.StartTime, particips]
			# p(rej)

			f=open("quer.txt","w+")
			for h in stratresults:
				for v in h:
					f.write(str(v)+"\t")
				f.write("\n")
			f.close()
			rejection = []
			rej = []
			time = 0
			r=0
			limit = 5
			entropy = []
			#fix = True
			for query in stratresults:
			#	if(fix):
			#		rejection.append([float(query[1]),1])
			#		fix=False
				gq = False
				if( query[0] == 1):
					gq = True
				if( query[0] == 0):
					r = r+1
					gq=True
				for peer_parti in query[5]:
					if(peer_parti > len(participation)):
						print "Weird participation : ",peer_parti
					else:
						participation[peer_parti] = participation[peer_parti] + 1
				if(gq and float(query[1]) - time > limit):
					#rejection
					rejection.append([time,r])
					#rej.append(r)
					r=0
					
					#entropy
					ent = 0
					prob = [0] * len(participation)
					total_participations = sum(participation)
					for i in range(len(participation)):
						if(total_participations > 0):
							prob[i] = float(participation[i])/float(total_participations)
					for p in prob:
						if(p>0):
							ent = ent - p * math.log(p)
					entropy.append([time,ent])
					participation = [0]*(1+Settings.peers)
					
					time = float(query[1])

			# p(latency)

			latency = []
			lat = []
			for query in stratresults:
				if( query[0] == 1 ):
					latency.append([float(query[1]) - startTime,float(query[4])])

			# p(saturation)

#			saturation = []
#			sat = []
#			participation = [0]*(1+Settings.peers)
			
#			for query in stratresults:
#				if(query[0] > -1 ):
#					s = 0
#					seeding = [int(i) for i in query[5]]
#					for p in seeding:
#						if(p > Settings.peers):
#							print "That's weird : ",p
#						else:
#					 		participation[p] = participation[p] + 1
			res = list()
			res.append(rejection)
			res.append(latency)
			res.append(entropy)
			metrics = ["rejection","latency","entropy"]
			#metrics = ["rejection"]
			metrics = ["rejection","entropy"]

			x=[]
			y=[]
			
			for metric in range(len(metrics)):
				try:
					print "Working on ",metrics[metric]
					x=[]
					y=[]

					maxval = float(max([a[1] for a in res[metric]]))
					minval = float(min([a[1] for a in res[metric]]))

					for r in res[metric]:
						x.append(float(r[0]))
						if(minval == maxval):
							y.append(minval)
							#y.append(1)
						else:
							y.append(1.0 - (1.0*(float(r[1])-minval))/(1.0*(maxval-minval)))
	
					densites = []
					histo = []
					totalpop = sum(y)
					for val in y:
						dens = float(val)
						densites.append(float("{0:.4f}".format(dens)))
							
	
					totalden = len(densites)
					densites_set = list(set(densites))
					densites_set.sort()
	
					for val in densites_set:
						histo.append((densites.count(val))/float(totalden))
					f=open(str(strat_id) + metrics[metric]+"_histo.txt","w+")
					for h in histo:
						f.write(str(h)+"\n")
					f.close()
					f=open(str(strat_id) + metrics[metric]+"_densites.txt","w+")
					for d in densites_set:
						f.write(str(d)+"\n")
					f.close()
					
					if(len(densites_set) == 1):
						this_strat.append([histo[0],-42,-42])
						print "unique val : ", histo[0]
					elif(len(densites_set) == 2):
						this_strat.append([histo[0],histo[1],-42])
						print "dual val : ", histo[0]," ", histo[1]
					else:
						(mu,sigma,a) =  leastsq(GlobalMemo.residuals,[0.4,0.1,0.5],args=(histo,densites_set))[0]
						print "mu : ", mu, ", sigma : ", sigma,", a : ", a
						# Saving infos	
						this_strat.append([mu,sigma,a])
				except TypeError:
					print "wat"
					f=open("wat"+str(strat_id) + metrics[metric]+".txt","w+")
					f.write(str([str(toto) for toto in x]))
					f.write("\n")
					f.write(str([str(toto) for toto in y]))
					f.close()

			print this_strat
			GlobalMemo.normalized.append(this_strat)

		rejections = [ GlobalMemo.montecarlo([a[0]]) for a in GlobalMemo.normalized ]
		print "Rejections : ",rejections
		selected = list()
		for ploup in range(len(rejections)):
			if(rejections[ploup] == max(rejections)):
				selected.append([ploup,GlobalMemo.normalized[ploup]])
		print "Selected : ",selected
		#area = [GlobalMemo.montecarlo(n) for n in GlobalMemo.normalized]
		area = [[n[0],GlobalMemo.montecarlo(n[1])] for n in selected]
		print "Area : ",area
		area_vals = [ n[1] for n in area ]
		print "Montecarlos : ",area_vals
		max_a_posteriori = area[area_vals.index(max(area_vals))][0]
		print "Max a posteriori : ",max_a_posteriori
		f=open("selection.txt","a+")
		f.write(str(GlobalMemo.normalized)+ "\n")
		f.write(str(area) + "\n")
		f.write(str(GlobalMemo.strategies[max_a_posteriori])+"\n")
		f.close()
		GlobalMemo.init()
		return GlobalMemo.strategies[max_a_posteriori]
