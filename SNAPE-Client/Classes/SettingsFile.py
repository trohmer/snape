#!/usr/bin/env python
# encoding: utf-8
"""
Settings.py

Created by Thibaud Rohmer on 2011-11-11.
Copyright (c) 2011 . All rights reserved.
"""


class Settings:

	# Number of peers
	peers	=	10
	
	# Id of the experience
	exp_id	=	"0"
	
	# Simulator Speed
	speed	=	1
	
	# Verbose level
	verbose	=	2

	# Server
	server 	=	'localhost'
	
	# Port
	port	=	1664
	
	# Log to File ?
	toFile	=	1
	
	# For errors
	ErrorFile	=	""
	
	# Simulation number
	simnum = 0

	# Type
	t = 0
