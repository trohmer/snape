
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.

from twisted.internet import pollreactor
pollreactor.install()

from twisted.internet import reactor, protocol, defer
import sys
import os
import time
import random as rd
import cPickle as pickle
sys.path.append('.');

### My Classes ###
from Verbose import *
from SettingsFile import *
from Logs import *
from Memory import *

class Trace():
	trace= []
	peers= []
	freq = 10
	ClientsConnected=0

	@classmethod
	def start(cls):
		Trace.ClientsConnected += 1
		if(Trace.ClientsConnected == Settings.peers):
			if(Settings.t == 1):
				pass
			else:
				Trace.req()

	@classmethod
	def req(cls):
		if(len(Trace.trace[0]) == 0):
			Trace.trace.pop(0)
			if(len(Trace.trace) == 0):
				rd.choice(Trace.peers).proto.sendEnd()
				return
			Trace.freq = float(3600) / (Settings.speed * len(Trace.trace[0]))
			rd.choice(Trace.peers).proto.sendNewHour()
			
		rd.choice(Trace.peers).proto.requestContent(Trace.trace[0].pop(0))
		
		reactor.callLater(Trace.freq,Trace.req)

	@classmethod 
	def random_weighted(cls,weights,s):
		rnd = rd.random() * s
		for i,w in enumerate(weights):
			rnd -= int(w)
			if (rnd < 0):
				return i

	@classmethod
	def generateNew(cls,querytable):
		daily_table = [2000,4000,5000,3000,1000,5000,4000,1000]
		print "Generating Trace"

		s = sum([int(i) for i in querytable])

		trace_f=open("./trace_generated.txt","w+")
		for i in range(len(daily_table)):
			old=0
			for j in range(daily_table[i]):
				myval = Trace.random_weighted(querytable,s)
				trace_f.write(str(myval)+" ")
			old=daily_table[i]+old
			trace_f.write("\n")
		trace_f.close()
		Trace.trace = [line[:-1].split(' ') for line in open('./trace_generated.txt')]
		print "Trace Generated"
		Trace.req()

	@classmethod
	def generateNew2(cls,hour):
		print "Generating Predicted Trace"
		trace_f = open("./trace_generated.txt","w+")
		base_trace = [line for line in open('./trace.txt')]
		for line in base_trace[(hour):(hour+6)]:
			trace_f.write(line)
		trace_f.close()
		Trace.trace = [line[:-1].split(' ') for line in open('./trace_generated.txt')]
		print "Trace Generated"
		Trace.req()

class QuerySave:
	def __init__(self,ContentId):
		self.ContentId	= ContentId
		self.TimeStamp	= time.time()


class PeerInfo():
	
	id=0
	
	def __init__(self,id):
		self.id 		= id
		self.mac 		= self.IntToMac(self.id)
		self.requests 	= []
			

	def _MacToInt(self,Mac):
		Id	=	0
		for val in Mac.split(":"):
			Id =  Id + int(val)
		return str(Id)

	@classmethod
	def MacToInt(self,mac):
		i=0
		for n in mac.split(':'):
			i=99*i+int(n)
		return i	

	@classmethod
	def _IntToMac(self, intmac):
		mac="%012d" % intmac
		for i in range(1,6):
			mac=mac[:12-2*i]+':'+mac[12-2*i:]
		return mac

	@classmethod
	def IntToMac(self, intmac):
			octet = ''
			for exp in [5,4,3,2,1,0]:
					if(intmac / (99 ** exp) < 10):
							octet = octet + "0" + str(intmac / (99 ** exp)) + ":"
					else:
							octet = octet + str(intmac / (99 ** exp)) + ":"
					intmac = intmac % ( 99 ** exp )
			return(octet.rstrip(':'))
	
	def IntToIp(self, intip ):
		octet = ''
		for exp in [3,2,1,0]:
			octet = octet + str(intip / ( 256 ** exp )) + "."
			intip = intip % ( 256 ** exp )
		return(octet.rstrip('.'))	

class PeerProtocol(protocol.Protocol):
	
	def __init__(self):
		pass
	
	def connectionMade(self):
		self.peer = self.factory.peer
		self.factory.proto = self
		pass
	

	def dataReceived(self, data):
		# Display
		Verbose(str(self.peer.id)+" received "+data,0)

		# Parse received info
		parsed	= data.split(" ")
		code	= parsed[0]

		# 100 Who?
		if(code == '100'):
			self.send('IDEN '+self.peer.mac)

		#101 Alive?
		if(code == '101'):
			self.send('ALIVE')

		#110 Welcome
		if(code == '110'):
			Verbose(str(self.peer.id)+" welcomed",2)
			Trace.start()
			
		#120 Ping
		if(code == '120'):
			self.send('PONG')

		# 220 LocList
		if(code == '220'):
			Verbose("\033[92m " + str(self.peer.id)+" got success for "+str(parsed[2])+" \033[0m",4)
			
			lines 	= data.split("\n")
			parsed	= lines.pop(0).split(" ")
			
			TimeStamp = self.getTimeStamp(parsed[2]) - Logs.StartTime
			l = Log("LOC", self.peer.id,Settings.speed * TimeStamp, parsed[2], parsed[3], time.time() - TimeStamp - Logs.StartTime)

			reactor.callLater( 60 * int(parsed[4]) / Settings.speed, self.sendAck, parsed[2])
			
			partic = []
			for line in lines[:-1]:
				parsed = line.split(" ")
				if(parsed[2] == "Content"):
					print "Weird : ", parsed
				else:
					l.ParticipIds.append(IPToInt(parsed[2]))	
					if(len(parsed) > 10):
						l.Particips.append(parsed[10])
					partic.append(IPToInt(parsed[2]))

			StratMemo.queries.append([1, TimeStamp,self.peer.id,parsed[1], time.time() - TimeStamp - Logs.StartTime, l.ParticipIds])
			l.save()



		#521 ContentOverloaded ContentId
		if(code == '521'):
			TimeStamp = self.getTimeStamp(parsed[1]) - Logs.StartTime
			Log("FAIL", self.peer.id,Settings.speed * TimeStamp,parsed[1], None, time.time() - TimeStamp - Logs.StartTime)
			StratMemo.queries.append([0, TimeStamp,self.peer.id,parsed[1], time.time() - TimeStamp - Logs.StartTime, [] ])
			Verbose("\033[91m " + str(self.peer.id) + " got failure for "+str(parsed[1]) + "\033[0m",4)

		if(code == '402'):
			Log("ABG",str(parsed[3]),str(parsed[2]),str(parsed[4]))
			Verbose("\033[94m new ABG "+str(parsed[2]) + " " + str(parsed[3]) + " " + str(parsed[4]) + " \033[0m",4)
			
			
		#777 Restart trace
		if(code == '777'):
			print "Received 777"
			GlobalMemo.new()
			Trace.trace = [line[:-1].split(' ') for line in open('./trace_generated.txt')]
			Trace.req()
		
		#666 End of sim, wait for next one
		if(code == '666'):
			print "Received 666"
			(a,b,g) = GlobalMemo.evaluate_plausibility()
			#(a,b,g) = GlobalMemo.evaluate()
			# Settings.intercomm.abg(parsed[1],parsed[2],parsed[3])
			Settings.intercomm.abg(a,b,g)
			return

	def getTimeStamp(self,ContentId):
		for req in self.peer.requests:
			if(req.ContentId == ContentId):
				ts=req.TimeStamp
				self.peer.requests.remove(req)
				return req.TimeStamp
				
		return time.time()

	def sendAck(self,ContentId):
		TimeStamp = self.getTimeStamp(int(ContentId)) - Logs.StartTime
		Verbose(str(self.peer.id)+" finished watching " + ContentId,3)
		Log("END", self.peer.id,Settings.speed * TimeStamp,ContentId)
		#StratMemo.queries.append([-1, TimeStamp,self.peer.id,ContentId, time.time() - TimeStamp - Logs.StartTime])
		self.send("ACK 40 "+ContentId)	

	def sendNewHour(self):
		Verbose("New Hour",5)
		self.send("HOUR")
		
	def sendEnd(self):
		Verbose("End of trace",5)
		if(Settings.t != 1):
			return
		
		self.send("CLEAR \n")
		tosend = "CLEAR \n"		
		tosend = ""
		print "Sending CLEAR"
		GlobalMemo.currentSim = GlobalMemo.currentSim + 1
		if(GlobalMemo.currentSim == 3):
			self.transport.write(tosend)
			GlobalMemo.currentSim = 0
			(a,b,g) = GlobalMemo.evaluate_plausibility()
			Settings.intercomm.abg(a,b,g)
		else:
			(a,b,g) = GlobalMemo.new()
			tosend = tosend +  "MYABG "+str(a)+" "+str(b)+" "+str(g)+" "+"\n"
			print "Sending ABG"
			rd.choice(Trace.peers).proto.send(tosend)
			Trace.trace = [line[:-1].split(' ') for line in open('./trace_generated.txt')]
			Trace.req()
		

		#self.send("EOT")

	def requestContent(self,ContentId):
		self.peer.requests.append(QuerySave(ContentId))
		self.send("FIND "+ContentId)
		Verbose(str(self.peer.id)+" requested " + ContentId,3)

	def _requestContent(self):
		
		if(len(Trace.trace) == 0):
			self.transport.loseConnection()
			return

		if(len(Trace.trace[0]) == 0):
			Trace.trace.pop(0)
			if(len(Trace.trace) > 0):
				Trace.freq = ( 3600 * float(Settings.peers) / float(len(Trace.trace[0]) * Settings.speed) )
			else:
				self.transport.loseConnection()
				exit

		ContentId = Trace.trace[0].pop(0)
		try:
			int(ContentId)
		except ValueError:
			return self.requestContent()
		self.peer.requests.append(QuerySave(ContentId))
		self.send("FIND "+ContentId)
		reactor.callLater( Trace.freq,self.requestContent)
		Verbose(str(self.peer.id)+" requested " + ContentId,3)

	def send(self,val):
		self.transport.write(val)
		Verbose(str(self.peer.id)+" sending "+val,1)

	def connectionLost(self, reason):
		pass

	def startSimu(self,id):
		Peer(id)

class Peer(protocol.ClientFactory):
	protocol = PeerProtocol
	
	def __init__(self, id):
		self.peer = PeerInfo(id)
		Trace.peers.append(self)

		reactor.connectTCP(Settings.server,Settings.port,self)
		
		if(id < Settings.peers-1):
			reactor.callLater(0.005,Peer,id+1)
	
		
			#d = self.waitPlease(id + 1)
			#d.addCallback(self.createPeer)
	#	else:
	#		print "All peers connected"



	def clientConnectionLost(self, connector, reason):
		pass

	def clientConnectionFailed(self, connector, reason):
		print "======== CONNECTION FAILED ========"
		pass
