
from twisted.internet import reactor, protocol, defer
from twisted.enterprise import adbapi

import psycopg2
import sys
import os.path
sys.path.append('.');

from Peer import *
from Logs import *
from SettingsFile import *

class InterServ(protocol.Protocol):
	
	t = 0
	init=0

	
	def __init__(self):
		Settings.intercomm = self
		pass
	
	def myinit(self):
		pass
			
	def updateabg(self):
		pass
		
	# Server tries to connect
	def connectionMade(self):
		self.myinit()
		self.transport.write("WHO \n")

	# Node sends info
	def dataReceived(self, data):
		parsed = data.split(" ")
		
		code = parsed[0]
		
		if(code == "ME"):
			self.transport.write("YOP \n")
			print "Server connected"
			return

		if(code == "GO"):
			print "Received GO"
			#Trace.generateNew(parsed[1:-1])
			Trace.generateNew2(int(parsed[1]))
			return

	def abg(self,a,b,g):
		self.transport.write('ABG '+str(a)+' '+str(b)+' '+str(g)+' \n')
		Logs.ABG=[a,b,g]
		print 'Update : ABG = '+str(a)+' '+str(b)+' '+str(g)+' '
