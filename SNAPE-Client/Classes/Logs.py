#!/usr/bin/env python
# encoding: utf-8
"""
Stats.py

Created by Thibaud Rohmer on 2011-11-11.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import getopt
import threading
sys.path.append('.');

### My Classes ###
from SettingsFile import *

lock = threading.Lock()

def IPToInt( IP ):
	intip = 0
	for exp in IP.split('.'):
		intip = 256*intip + int(exp)
	return(intip)

class Log:
	
	# Called when received :
	# 220-Loclist ContentId ContentSize ContentDuration ContentAlgo ContentKey ContentPattern
	##########################
	def __init__(self, Type, PeerId, TimeStamp, ContentId, ContentSize=None, WaitingTime = None):
		self.Type		=	Type
		self.PeerId		=	PeerId
		self.TimeStamp	=	TimeStamp
		self.ContentId	=	ContentId
		
		if(Type != "LOC"):
			return self.save()
		
		self.WaitingTime=	WaitingTime
		self.ContentSize=	int(ContentSize)
		self.ParticipIds=	[]
		self.Particips	=	[]
		self.Completed	=	0
	
	def edit(self, ParticipIP, Particip):
		self.ParticipIds.append(int(IPToInt(ParticipIP)))
		self.Particips.append(int(Particip))
		self.Completed += int(Particip)
		
	def complete(self):
		return (self.ContentSize <= self.Completed)

	def save(self):
		logline =	str(self.Type) +	"\t"
		logline	+=	str(self.TimeStamp) + "\t"
		logline += 	str(self.PeerId) + "\t"
		logline +=	str(self.ContentId) + "\t"
		
		if(self.Type != "LOC"):
			logline += "\n"
			return self.toFile(logline)
			
		logline +=	str(self.WaitingTime) + "\t"
		for i in range(len(self.ParticipIds)):
			logline +=	str(self.ParticipIds[i]) + "\t"
#			logline +=	str(self.Particips[i]) + "\t"
		logline += "\n"
	
		if(Settings.toFile == 1):
			self.toFile(logline)
	
	def toFile(self,logline):
		with lock:
			(Logs.File).write(logline)
		
class Logs:
	logs	=	[]
	StartTime = 0
	File	=	""
	FileName=	"logs"

class LocListLogs:
	
	# Called when received :
	# 220-Loclist ContentId ContentSize ContentDuration ContentAlgo ContentKey ContentPattern
	##########################
	def create_Log(self,PeerId, TimeStamp, ContentId, ContentSize, WaitingTime):
		Logs.logs.append( Log("LOC",PeerId,TimeStamp,ContentId,ContentSize, WaitingTime ) )


	# Called when received :
	# 220-Peer Type PeerIP ContentId PartId FirstFrameFromPeer NumberOfPacketsFromPeer FirstFrame NumberOfPackets IperTenMilMovie
	########################	
	def edit_Log(self,PeerId,ParticipIP,Particip):
		for log in Logs.logs:
			if(log.PeerId == PeerId):
				log.edit(ParticipIP,Particip)
				if(log.complete()):
					log.save()
					Logs.logs.remove(log)
