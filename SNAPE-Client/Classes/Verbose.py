#!/usr/bin/env python
# encoding: utf-8
"""
Verbose.py

Created by Thibaud Rohmer on 2011-11-11.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import getopt

### My Classes ###
from SettingsFile import *

# Callable class
class Verbose():
		
	def __init__(self,value,level):
		# Test level
		if ( level >= Settings.verbose):
			# output value
			print value
