#!/usr/bin/env python
# encoding: utf-8
"""
Simulator.py

Created by Thibaud Rohmer on 2011-11-11.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import getopt
import time
import random
import traceback
import threading
sys.path.append('.');

### My Classes ###
from Peer import *
from SettingsFile import *
from Logs import *



class Simulator():
	
	# Array containing our Peers (threads)
	peers		=	[]

	# Keep on running 
	running		=	1
	
	# Hour...
	hour 		=	0
	
	# Create Simulator #
	def __init__(self):
		
		# Parse trace
		Trace.trace = [line[:-1].split(' ') for line in open('./trace.txt')]
		
		# Calculate frequency
		Trace.freq = ( float(3600) / float(len(Trace.trace[0]) * Settings.speed) )

		Verbose("Connecting peers",5);
		
		# Create peers
		Peer(0)
		
		Verbose("Done.",5);
		t=Trace()
		t.start()
		# Setup StartTime
		Logs.StartTime = time.time()

