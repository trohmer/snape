import sys
import getopt
import os
import math

sys.path.append('./Classes');

### My Classes ###
from Peer import *
from Settings import *

class NoSpace:
	pass

class AlikeDispatch:
	
	def __init__(self):
		self.peers = [Peer() for i in range(Settings.peers)]
		self.spaceLeft = [[] for i in range(Settings.peersize+1) ]
		self.spaceLeft[-1] = self.peers
		
		# Taking care of each content
		for i in range(Settings.contents):
			# Look for a space
			where = self.whereContent(Settings.ava[i] / Settings.parts)

			# Insert content
			self.insertContent(i,Settings.ava[i],where)
			
			
	def whereContent(self,contentSpread):
		arr = []
		
		for i in range(len(self.spaceLeft)-1,0,-1):
			# If we have as many node with this space available as number of parts
			if(len(self.spaceLeft[i]) == contentSpread):
				arr = self.spaceLeft[i]
				self.spaceLeft[i-1] = self.spaceLeft[i-1].extend(self.spaceLeft[i])
				self.spaceLeft[i]	= []
				return self.spaceLeft[i]

		
		for i in range((len(self.spaceLeft))-1,0,-1):				
			# If we have a combination available
			for j in range((len(self.spaceLeft))-1,i,-1):
				if(len(self.spaceLeft[i]) + len(self.spaceLeft[j]) == contentSpread):
					arr1 = self.spaceLeft[i]
					arr2 = self.spaceLeft[j]
					
					self.spaceLeft[i] = []
					self.spaceLeft[j] = []
					
					self.spaceLeft[i-1] = self.spaceLeft[i-1].extend(arr1)
					self.spaceLeft[j-1] = self.spaceLeft[j-1].extend(arr2)
					return arr1.extend(arr2)

		for i in range(len(self.spaceLeft)-1,0,-1):
			# We take the ones with the most space
			if(len(arr)+len(self.spaceLeft[i]) >= contentSpread):

				# We pop the nodes
				nodespopped = self.spaceLeft.pop(i)
				self.spaceLeft.append([])
				# We put back the "too much"
				for j in range(len(nodespopped) - contentSpread):
					self.spaceLeft[i].append(nodespopped.pop())

				arr.extend(nodespopped)
				self.spaceLeft[i-1].extend(nodespopped)
				return arr

			arr.extend(self.spaceLeft[i])
			
		raise NoSpace
		
		
		
	def insertContent(self, contentId, contentSpread, where):
		print contentSpread/Settings.parts
		print len(where)
		for i in range(Settings.parts):
			for j in range(contentSpread/Settings.parts):
				where[j].add(str(contentId),i,contentSpread)
			
			
	def toSQL(self,f):
		for peer in self.peers:
			peer.toSQL(f)
			
			
			
	