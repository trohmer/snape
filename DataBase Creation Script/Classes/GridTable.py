#!/usr/bin/env python
# encoding: utf-8
"""
peer.class.py

Created by Thibaud Rohmer on 2011-11-09.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import os
sys.path.append('.')

from Grid import *


class GridTable:
	id=0
		
	def __init__(self):
		self.grids	=	[]
		self.space	=	0
		self.id		=	GridTable.id
		GridTable.id=	GridTable.id+1
		pass

	def add(self,ContentId,PartId,ContentDispat):
		
		if(	self.space == 0 ):
			self.grids.append(Grid(ContentDispat))
			self.space = self.grids[-1].space
			print "Creating new grid. GridTable space : " + str(self.space)
		for grid in self.grids:
			if( grid.space > 0 ):
				try:
					grid.add(ContentId,PartId)
					self.space	=	self.space - 1
					return
				except GridException:
					print "Got a GridException. But it wa nicely caught !"

		## Creating a new grid
		self.grids.append(Grid(ContentDispat))
		self.space = self.space + self.grids[-1].space
		print "Creating new grid. GridTable space : " + str(self.space)
		self.grids[-1].add(ContentId,PartId)
		self.space	=	self.space - 1			

	def toSQL(self,f):
		print "gridtable"
		for grid in self.grids:
			grid.toSQL(f)


	def toHTML(self,f):
		f.write("<div class='gridtable'>\n")
		for grid in self.grids:
			grid.toHTML(f)
		f.write("</div>\n")

	def toDot(self,f):
		f.write('strict digraph Dispatch{\n\
		graph [	label="Similarity Dispatching"];\n\
		{\n')
		f.write("gridtable_"+str(self.id)+" [label="+str(self.id)+",color=red]\n")
		for grid in self.grids:
			f.write( "gridtable_"+str(self.id)+"->grid_"+grid.toDot(f)+"\n")
		f.write("}\n}");
		
		return str(self.id)