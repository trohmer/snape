#!/usr/bin/env python
# encoding: utf-8
"""
settings.class.py

Created by Thibaud Rohmer on 2011-11-09.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import os
sys.path.append('.')

class Settings:

	peers		=	100
	peersize 	= 	100
	contents	=	100
	parts		=	5
	dbid		=	"0"
	pop 		=	[]
	ava 		=	[]


	@classmethod
	def availability_old(cls):
		for popularity in Settings.pop:
			Settings.ava.append( Settings.pop2ava(popularity) )

	@classmethod
	def availability(cls):
		for i in range(len(Settings.pop)):
			if( i * 100 < len(Settings.pop) * 20 ):
				Settings.ava.append( 80.0 * Settings.peers * Settings.peersize / ( 100.0 * len(Settings.pop) * 20.0 / 100.0 ) )
			else:
				Settings.ava.append( 20.0 * Settings.peers * Settings.peersize / ( 100.0 * len(Settings.pop) * 80.0 / 100.0 ) )

	@classmethod
	def pop2ava(cls, p):
		av = ( float(p) * (Settings.peers * Settings.peersize) / sum(Settings.pop) )
		if(av > Settings.peers * Settings.parts):
			av = Settings.peers * Settings.parts
		return int(av)


