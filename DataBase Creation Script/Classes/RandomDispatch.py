import sys
import getopt
import os
import math
import random
import psycopg2


sys.path.append('./Classes');

### My Classes ###
from Peer import *
from SettingsModule import *

class NoSpace:
	pass

class RandomDispatch:
	
	def __init__(self):
		self.peers = [Peer() for i in range(Settings.peers)]
		self.avapeers = [p for p in self.peers]
		
		
		# Taking care of each content
		for i in range(Settings.contents):
			print str(i)+"/"+str(Settings.contents)
			for j in range(Settings.ava[i]):
				p = self.avapeers.pop(random.randint(0,len(self.avapeers) - 1 ))
				#p = random.choice(self.avapeers)
				p.add(i,j % Settings.parts,Settings.ava[i])
				if(p.space > 0):
					self.avapeers.append(p)
				
	def toSQL(self,f):
		for peer in self.peers:
			peer.toSQL(f)

	def toDB(self,f):
		for peer in self.peers:
			peer.toDB(f)
