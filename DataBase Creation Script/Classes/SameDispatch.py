import sys
import getopt
import os
import math
import random
import bisect

sys.path.append('./Classes');

### My Classes ###
from Peer import *
from SettingsModule import *

class NoSpace:
	pass

class Content:
	def __init__(self,id):
		self.id	 = id;
		self.ava = Settings.ava[id];


class SameDispatch:
	
	def __init__(self):
		self.peers = [Peer() for i in range(Settings.peers)]
		self.avapeers = [p for p in self.peers]
		self.avacontents = [Content(i) for i in range(Settings.contents)]
		
		# Taking care of each content
		i=0
		while(len(self.avacontents) > 0):
			toAdd = []
			added = 0

			# get contents to fill the "line"
			for cont in self.avacontents:
				if(added + cont.ava <= len(self.avapeers)):
					toAdd.append(cont)
					added += cont.ava
					if(added == len(self.avapeers)):
						break

						
			i+=len(toAdd)
			print str(i)+"/"+str(Settings.contents)

			# fill the line
			for cont in toAdd:
				self.avacontents.remove(cont)

				for j in range(cont.ava):
					p = self.avapeers.pop(0)
					p.add(cont.id,j % Settings.parts,cont.ava)

					
					if(p.space > 0):
						bisect.insort_left(self.avapeers,p)
				
	def toSQL(self,f):
		for peer in self.peers:
			peer.toSQL(f)

	def toDB(self,f):
		for peer in self.peers:
			peer.toDB(f)
