#!/usr/bin/env python
# encoding: utf-8
"""
peer.class.py

Created by Thibaud Rohmer on 2011-11-09.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import os
import psycopg2

sys.path.append('.')
from SettingsModule import *

class PeerError:
	pass

class Peer:

	id		= 0
	have_id = 0
	
	def __init__(self):
		Settings()
		self.id			=	Peer.id;
		self.space		=	Settings.peersize
		self.parts		=	[]
		self.contents		=	[]
		self.cs			=	0
		self.sps		=	0
		
		Peer.id					=	Peer.id + 1
		if(self.id > Settings.peers):
			raise PeerError

	def __cmp__(self,other):
		return cmp(self.sps,other.sps)

	def add(self,ContentId,PartId,ContentSpread):
		PartId = str(ContentId)+"_"+str(PartId)
		self.parts.append(PartId)
		self.contents.append(ContentId)
		self.space	-=	1
		self.sps	+=	ContentSpread
		self.cs		+=	100.0 / ContentSpread
		if(self.space<0):
			raise PeerError
	
	
	def toSQL(self,f):
		comment = "INSERT INTO peer VALUES ({0},'{1}',1,2000,'{2}','{3}','pass',1000,1000,1,1,0,1,{4},{5});\n".format(
			self.id,self.id,self.IntToIp(self.id),self.IntToMac(self.id),self.cs,self.sps)
		
		for part in self.parts:
			comment=comment+"INSERT INTO have VALUES ( {0}, NULL, '{1}', {2} );\n".format( Peer.have_id , part, self.id )
			Peer.have_id=Peer.have_id+1			
		f.write(comment)
		
	def toDB(self,conn):
		
		peer_command = "INSERT INTO peer VALUES ({0},'{1}',1,2000,'{2}','{3}','pass',1000,1000,1,1,0,1,{4},{5});\n".format(
			self.id,self.id,self.IntToIp(self.id),self.IntToMac(self.id),self.cs,self.sps)
		have_command = "INSERT INTO have VALUES "
		for part in self.parts:
			have_command += " ( "+str(Peer.have_id)+", NULL, '"+part+"', '"+str(self.id)+"' ), \n"
			Peer.have_id += 1
		
		curr=conn.cursor()
		curr.execute(peer_command)
		curr.execute(have_command[:-3]+";\n")
		conn.commit()
		curr.close()
		
	def toDot(self,f):
		f.write("peer_"+str(self.id)+" [label="+str(self.id)+", color=brown]\n")
		p=open("Debug/p"+str(self.id)+".dot","w")
		for part in self.parts:
			f.write( "peer_"+str(self.id)+"->c"+self.partToDot(f,part)+"\n")
			p.write( "peer"+str(self.id)+"->"+"p"+self.partToDot(f,part)+"\n")
			c=open("Debug/c"+str(self.id)+".dot","a+")
			c.write("p"+self.partToDot(f,part)+"->peer"+str(self.id)+"\n")
			c.close()
		p.close()
		return str(self.id)

	def IntToMac(self, intmac):
			octet = ''
			for exp in [5,4,3,2,1,0]:
					if(intmac / (99 ** exp) < 10):
							octet = octet + "0" + str(intmac / (99 ** exp)) + ":"
					else:
							octet = octet + str(intmac / (99 ** exp)) + ":"
					intmac = intmac % ( 99 ** exp )
			return(octet.rstrip(':'))

	def IntToIp(self, intip ):
		octet = ''
		for exp in [3,2,1,0]:
			octet = octet + str(intip / ( 256 ** exp )) + "."
			intip = intip % ( 256 ** exp )
		return(octet.rstrip('.'))

	def num_peers(self):
		return Peer.id
		
	def partToDot(self,f,c):
		return c
