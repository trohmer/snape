#!/usr/bin/env python
# encoding: utf-8
"""
trace.py

Created by Thibaud Rohmer on 2011-11-09.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import os
import random
import math
sys.path.append('.')
from SettingsModule import *

class Trace:

	arg_a = []
	arg_b = []
	arg_c = []

	@classmethod
	def classic(cls):
		one_day = [1000]*6 + [5000]*6 + [3000]*6 + [5000]*6
		daily_table = one_day * 4
		maxnum=sum(Settings.pop)
		trace_table=[-1]*sum(Settings.pop)
		old=0
		for i in range(len(Settings.pop)):
			for j in range(Settings.pop[i]):
				trace_table[old+j]=i
			old+=Settings.pop[i]

		random.seed()
		trace_f=open("./trace.txt","w+")
		for i in range(len(daily_table)):
			old=0
			for j in range(daily_table[i]):
				myval=random.choice(trace_table)
				trace_f.write(str(myval)+" ")
			old=daily_table[i]+old
			trace_f.write("\n")
		trace_f.close()
		return

	@classmethod
	def update_func(cls,a,b,c,t):
		if(t==0):
			return a + c
		else:
			return a * math.exp( 2.0/math.sqrt(10) - float(t)/(10*b) - float(b)/t ) + c

	@classmethod
	def update_pop(cls,t):
		Settings.pop = [ Trace.update_func( Trace.arg_a[i],Trace.arg_b[i],Trace.arg_c[i],t ) for i in range(Settings.contents - 1)  ]

	@classmethod
	def updating(cls):
		one_day = [10000]*6 + [1000]*6 + [8000]*6 + [5000]*6
		one_day = [100]*96
		Trace.arg_a = Settings.pop
		Trace.arg_b = [random.randint(1,10)]*Settings.contents
		Trace.arg_c = [random.randint(90,100)]*Settings.contents

		daily_table = one_day * 4
		maxnum=sum(Settings.pop)
		trace_table=[-1]*sum(Settings.pop)
		old=0
		for i in range(len(Settings.pop)):
			for j in range(Settings.pop[i]):
				trace_table[old+j]=i
			old+=Settings.pop[i]

		random.seed()
		t=0
		d=0
		trace_f=open("./trace.txt","w+")
		for i in range(len(daily_table)):
			old=0
			for j in range(daily_table[i]):
				myval=random.choice(trace_table)
				trace_f.write(str(myval)+" ")
			t = t+1
			if(t==24):
				d=d+1
				t=0
				Trace.update_pop(t)
			old=daily_table[i]+old
			trace_f.write("\n")
		trace_f.close()
		return



	@classmethod
	def byCat(cls):
		dailytable 	= [
					[100,150,20,30,1000,5,10,0,500,500],
					[1300,100,200,3000,5,0,10,0,0,100],
					[2300,100,200,2000,10,0,5,5,5,0],
					[100,1000,1200,1000,100,50,150,0,50,0],
					[100,1000,200,1000,100,100,0,0,0,0],
					[1500,100,200,1200,0,0,0,0,100,0],
					[1500,100,200,1300,100,100,0,100,100,0],
				  ]

		maxnum		=	sum(Settings.pop)


		numcat 			= len(dailytable[0])
		contents_table 		= [[]]*numcat
		trace 			= []
		random.seed()

		# Build Contents table
		for i in range(len(Settings.pop)):
			for j in range(Settings.pop[i]):
				contents_table[i%numcat].append(i)

		# Each hour
		for i in range(len(dailytable)):
			subtrace = [] * sum(dailytable[i])
			demands = dailytable[i]

			# Each category
			for cat in range(len(demands)):
				# Each demand
				for d in range(demands[cat]):
					v=random.choice(contents_table[cat])
					subtrace.append(v)
		
			random.shuffle(subtrace)
			trace.append(subtrace)

		# Write to file
		f = open("./trace.txt","w+")
		for hour in trace:
			for d in hour:
				f.write(str(d)+" ")
			f.write("\n")

		return




