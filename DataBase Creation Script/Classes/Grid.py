#!/usr/bin/env python
# encoding: utf-8
"""
peer.class.py

Created by Thibaud Rohmer on 2011-11-09.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import os
import pprint

sys.path.append('.')
from SubGrid import *
from Settings import *

# Called when we couldn't put a part in a peer that didn't already have it
class GridException:
	pass

class Grid:
	
	id = 0
	
	def __init__(self,ContentDispatch):
		Settings()
		self.id			=	Grid.id
		self.space		=	0
		self.parts		=	[]
		self.contents	=	[]
		
		# if we are going to go over the top
		if(Settings.current_peers + Settings.parts_per_content * ContentDispatch > Settings.peers):
			new_size = int((Settings.peers - Settings.current_peers) / Settings.parts_per_content)
			self.subgrids	=	[SubGrid(new_size) for i in range(Settings.parts_per_content)]
		else:
			self.subgrids	=	[SubGrid(ContentDispatch) for i in range(Settings.parts_per_content)]

		for subgrid in self.subgrids:
			self.space 		=	self.space + subgrid.space	
		self.selected_peer		=	0
		self.selected_subgrid	=	0
		print "Creating new grid. Grid space : " + str(self.space)
		
		Grid.id	=	Grid.id + 1

	def add(self,ContentId,PartId):
		self.select(ContentId,PartId)
		self.selected_subgrid.add(ContentId,PartId)
		self.selected_peer.add(ContentId,PartId)

	
	def select(self,ContentId,PartId):
		free_subgrids=[]
		
		# We work only with the subgrids with free space
		for subgrid in self.subgrids :
			if ( subgrid.space > 0 ):
				free_subgrids.append(subgrid)

		# Look for a subgrid with this part
		for subgrid in free_subgrids :
			if( PartId in subgrid.parts ):
				# Look for a peer with space
				for peer in subgrid.peers :
					if ( peer.space > 0 and not ( PartId in peer.parts ) ):
						self.selected_peer 		=	peer
						self.selected_subgrid	=	subgrid
						return

		## This part cannot be put with its siblings ##
		
		# Select the first subgrid that doesn't have any part of this content
		for subgrid in free_subgrids :
			if (not ( ContentId in subgrid.contents )):
				# Look for a peer with space
				for peer in subgrid.peers :
					if( peer.space > 0 ):
						self.selected_peer = peer
						self.selected_subgrid = subgrid
						return
					
		## This part has to be placed with another part of the same content ##
			
		# Those subgrids have the content, we look for a peer that doesnt have it
		for subgrid in free_subgrids :
			# Look for a peer with free space, without the content
			for peer in subgrid.peers :
				if ( peer.space > 0 and not ( ContentId in peer.contents )):
					self.selected_peer = peer
					self.selected_subgrid = subgrid
					return
			# Last resort : just look for free space
			for peer in subgrid.peers :
				# Peer without the part, with space
				if ( peer.space > 0 and not ( PartId in peer.parts )):
					self.selected_peer = peer
					self.selected_subgrid = subgrid
					return

		## This part can't be put in any peer that doesn't have it ##
		## If there is still place for a new Grid ##
		if(self.subgrids[0].peers[0].num_peers() < Settings.peers):
			print "There is still space..."
			raise GridException
		else:
			for subgrid in free_subgrids :
				for peer in subgrid.peers :
					if(peer.space > 0):
						self.selected_peer = peer
						self.selected_subgrid = subgrid
						return
		print "WTF"


		

	def toSQL(self,f):
		for subgrid in self.subgrids:
			subgrid.toSQL(f)	

	def toDot(self,f):
		f.write("grid_"+str(self.id)+" [label="+str(self.id)+", color=blue]\n")
		for subgrid in self.subgrids:
			f.write( "grid_"+str(self.id)+"->subgrid_"+subgrid.toDot(f)+"\n")
		return str(self.id)
