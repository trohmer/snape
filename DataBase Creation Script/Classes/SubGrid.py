#!/usr/bin/env python
# encoding: utf-8
"""
subgrid.class.py

Created by Thibaud Rohmer on 2011-11-09.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import os

sys.path.append('.')
from Peer import *

class SubGridError:
	pass

class SubGrid:
	
	id = 0
	
	def __init__(self,ContentDispatch):
		Settings()
		self.id			=	SubGrid.id
		self.parts		=	[]
		self.contents	=	[]
		if(ContentDispatch + Peer.id > Settings.peers):
			self.space		=	(Settings.peers - Settings.current_peers) * Settings.peersize
			self.peers		=	[Peer() for i in range(Settings.peers - Peer.id)]
		else:
			self.space		=	ContentDispatch * Settings.peersize
			self.peers		=	[Peer() for i in range(ContentDispatch)]

		print "Creating new SubGrid. SubGrid space : " + str(self.space)
		
		SubGrid.id	=	SubGrid.id + 1
	
	def add(self,ContentId,PartId):
		self.parts.append(PartId)
		self.contents.append(ContentId)
		self.space	=	self.space - 1
		if(self.space<0):
			raise SubGridError
		
	def toSQL(self,f):
		for peer in self.peers:
			peer.toSQL(f)
			
	def toDot(self,f):
		f.write("subgrid_"+str(self.id)+" [label="+str(self.id)+"-"+str(len(self.peers))+", color=yellow]\n")
		for peer in self.peers:
			f.write( "subgrid_"+str(self.id)+"->peer_"+peer.toDot(f)+"\n")
		return str(self.id)
