#!/usr/bin/env python
# encoding: utf-8
"""
main.py

Created by Thibaud Rohmer on 2011-11-18.
Copyright (c) 2011 . All rights reserved.
"""

import sys
import getopt
import os
import math
import psycopg2

sys.path.append('./Classes');

### My Classes ###
from Peer import *
from RandomDispatch import *
from PWDispatch import *
from SameDispatch import *
from SettingsModule import *
from TraceModule import *

help_message = '''
c:contents
p:peers
s:peersize
t:trace
'''


class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg


def main(argv=None):
	if argv is None:
		argv = sys.argv
	try:
		try:
			opts, args = getopt.getopt(argv[1:], "tc:u:p:s:ho:v", ["help", "output="])
		except getopt.error, msg:
			raise Usage(msg)
		
		trace = False
		# option processing
		for option, value in opts:
			if option == "-v":
				verbose = True
			if option in ("-h", "--help"):
				raise Usage(help_message)
			if option in ("-o", "--output"):
				output = value
			if option == "-c":
				Settings.contents = int(value)
			if option == "-p":
				Settings.peers = int(value)
			if option == "-s":
				Settings.peersize = int(value)
			if option == "-u":
				Settings.dbid = value
			if option == "-t":
				trace = True
	
	except Usage, err:
		print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
		print >> sys.stderr, "\t for help use --help"
		return 2
	# Popularity

	poponly = False
	rareonly = True
	#Settings.pop = [(int)(500*math.exp(-15*(i*i)/(float)(Settings.contents*Settings.contents))) for i in range(Settings.contents)]
	if(poponly):
		Settings.contents = Settings.contents * 20 / 100
	if(rareonly):
		new =  Settings.contents * 80 / 100
		missing = Settings.contents - new
		Settings.contents = new

	C = 1.0/sum([1.0/(i**1) for i in range(1,Settings.contents+1)])
	Settings.pop = [ (int)(50000.0 * C / (i**1)) for i in range(1,Settings.contents+1) ]
	print Settings.pop

	if(rareonly):
		Settings.contents = Settings.contents + missing
		Settings.pop = [0]*missing + Settings.pop
	# Trace
	if(trace):
		Trace.classic()
		#Trace.updating()
		exit(0)

	# Availability
	Settings.availability()
	print Settings.ava
	print sum(Settings.ava)
	# Contents
	conn = psycopg2.connect("host=localhost dbname=supernode_"+Settings.dbid+" user=supernode password=PassNodeSuper")
	cur = conn.cursor()
	
	for i in ['uploaded','downloaded','locpeer','locpeerlog','have','part','content','peer']:
		cur.execute("DELETE FROM "+i+";");
	conn.commit()
	cur.close()
	cur = conn.cursor()
	
	print "inserting..."
	cont_command="INSERT INTO content VALUES \n"
	part_command="INSERT INTO part VALUES \n"
	for i in range(Settings.contents):
		
		cont_command += " ('"+str(i)+"', 0, 'dvd', '120', 'AES', 'cypher_key', 'ALL'), \n"
		for j in range(Settings.parts):
		
			SettingstId = str(i)+"_"+str(j)
			part_command += "( 0, "+str(j)+", '"+SettingstId+"', NULL, 'd41d8cd98f00b204e9800998ecf8427e', 58333, 10, 200, '"+str(i)+"'), \n"
	
	
	cur.execute(cont_command[:-3]+";\n")
	cur.execute(part_command[:-3]+";\n")
	
	conn.commit()
	cur.close()
	
	# Do dispatching
	
	
	print "dispatching..."
	#disp = AlikeDispatch()
	#disp = RandomDispatch()
	disp = PWDispatch()
	#disp = SameDispatch()
	
	
	# Output 
	disp.toDB(conn)
	
		
if __name__ == "__main__":
	sys.exit(main())
